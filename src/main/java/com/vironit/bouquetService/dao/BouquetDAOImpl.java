package com.vironit.bouquetService.dao;

import com.vironit.bouquetService.dao.interfaces.BouquetDAO;
import com.vironit.bouquetService.model.Bouquet;
import com.vironit.bouquetService.util.HibernateSessionFactoryUtil;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.sql.*;
import java.util.List;

public class BouquetDAOImpl implements BouquetDAO {

    private static final Logger log = Logger.getLogger(BouquetDAOImpl.class);

    @Override
    public void add(Bouquet bouquet) throws SQLException{

        try {
            Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
            Transaction transaction = session.beginTransaction();
            session.save(bouquet);
            transaction.commit();
            session.close();
        } catch (Exception e){
            e.printStackTrace();
            log.error(e);
        }
    }

    @Override
    public List<Bouquet> getAll() throws SQLException{
        String hql = "FROM Bouquet B ORDER BY B.id";
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery(hql);
        List<Bouquet> bouquets = query.list();
        transaction.commit();
        session.close();
        return bouquets;
    }

    @Override
    public Bouquet getById(long id) throws SQLException{
        Bouquet bouquet;
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        bouquet = session.get(Bouquet.class, id);

        transaction.commit();
        session.close();
        return bouquet;
    }

    @Override
    public void update(Bouquet bouquet) throws SQLException{
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        session.update(bouquet);

        transaction.commit();
        session.close();
    }

    @Override
    public void remove(long id) throws SQLException{
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        Bouquet bouquet = (Bouquet) session.get(Bouquet.class, id);
        session.delete(bouquet);

        transaction.commit();
        session.close();
    }
}
