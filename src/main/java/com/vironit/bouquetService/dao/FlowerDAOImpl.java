package com.vironit.bouquetService.dao;

import com.vironit.bouquetService.dao.interfaces.FlowerDAO;
import com.vironit.bouquetService.model.Flower;
import com.vironit.bouquetService.util.HibernateSessionFactoryUtil;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.sql.*;
import java.util.List;

public class FlowerDAOImpl implements FlowerDAO {

    private Logger log = Logger.getLogger(FlowerDAOImpl.class);

    @Override
    public void add(Flower flower) throws SQLException {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(flower);
        transaction.commit();
        session.close();
    }

    @Override
    public List<Flower> getAll() throws SQLException {
        String hql = "FROM Flower F ORDER BY F.name";
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery(hql);
        List<Flower> flowers = query.list();
        transaction.commit();
        session.close();
        return flowers;
    }

    @Override
    public Flower getById(long id) throws SQLException {
        Flower flower ;
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        flower = session.get(Flower.class, id);

        transaction.commit();
        session.close();
        return flower;
    }

    @Override
    public void update(Flower flower) throws SQLException {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.update(flower);
        } catch (RuntimeException e){
            log.error(e.getMessage());
            transaction.rollback();
        }
        transaction.commit();
        session.close();
    }

    @Override
    public void remove(long id) throws SQLException {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        Flower flower = (Flower) session.get(Flower.class, id);
        session.delete(flower);

        transaction.commit();
        session.close();
    }
}
