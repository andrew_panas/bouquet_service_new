package com.vironit.bouquetService.dao;

import com.vironit.bouquetService.dao.interfaces.OrderDAO;
import com.vironit.bouquetService.model.Order;
import com.vironit.bouquetService.util.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;


import java.sql.*;
import java.util.List;

public class OrderDAOImpl implements OrderDAO {


    @Override
    public void add(Order order) throws SQLException {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(order);
        transaction.commit();
        session.close();
    }

    @Override
    public List<Order> getAll() throws SQLException {
        String hql = "FROM Order O ORDER BY O.id";
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery(hql);
        List orders = query.list();
        transaction.commit();
        session.close();
        return orders;
    }

    @Override
    public Order getById(long id) throws SQLException{
        Order order;
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        order = session.get(Order.class, id);

        transaction.commit();
        session.close();
        return order;
    }

    @Override
    public void update(Order order) throws SQLException {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        session.update(order);

        transaction.commit();
        session.close();
    }

    @Override
    public void remove(long id) throws SQLException {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        Order order = (Order) session.get(Order.class, id);
        session.delete(order);

        transaction.commit();
        session.close();
    }
}
