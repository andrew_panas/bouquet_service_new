package com.vironit.bouquetService.dao;

import com.vironit.bouquetService.dao.interfaces.UserDAO;
import com.vironit.bouquetService.model.User;
import com.vironit.bouquetService.util.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.sql.*;
import java.util.List;

public class UserDAOImpl implements UserDAO{

    @Override
    public void add(User user) throws SQLException{
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(user);
        transaction.commit();
        session.close();
    }

    @Override
    public List<User> getAll() throws SQLException{
        String hql = "FROM User U ORDER BY U.id";
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery(hql);
        List users = query.list();
        transaction.commit();
        session.close();
        return users;
    }

    @Override
    public User getById(long id) throws SQLException{
        User user;
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        user = session.get(User.class, id);

        transaction.commit();
        session.close();
        return user;
    }

    @Override
    public void update(User user) throws SQLException{
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        session.update(user);

        transaction.commit();
        session.close();
    }

    @Override
    public void remove(long id) throws SQLException{
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        User user = (User) session.get(User.class, id);
        session.delete(user);

        transaction.commit();
        session.close();
    }
}
