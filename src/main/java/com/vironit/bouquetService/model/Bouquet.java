package com.vironit.bouquetService.model;

import com.vironit.bouquetService.model.enums.BouquetType;

import javax.persistence.*;
import java.util.List;


@Entity(name = "Bouquet")
@Table(name = "bouquet")
public class Bouquet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "bouquet_id")
    private long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private BouquetType type;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fk_ord", nullable = false)
    private Order order;

    @OneToMany(mappedBy = "bouquet", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<FlowerInBouquet> flowerInBouquets;


    public Bouquet(BouquetType type) {
        this.type = type;
    }

    public Bouquet() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BouquetType getType() {
        return type;
    }

    public void setType(BouquetType type) {
        this.type = type;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public List<FlowerInBouquet> getFlowerInBouquets() {
        return flowerInBouquets;
    }

    public void setFlowerInBouquets(List<FlowerInBouquet> flowerInBouquets) {
        this.flowerInBouquets = flowerInBouquets;
    }

    @Override
    public String toString() {
        return "Bouquet{" +
                "id=" + id +
                ", type=" + type +
                '}';
    }
}
