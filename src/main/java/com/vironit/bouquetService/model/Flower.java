package com.vironit.bouquetService.model;

import com.vironit.bouquetService.model.enums.FlowerColor;
import com.vironit.bouquetService.model.enums.FlowerName;

import javax.persistence.*;
import java.util.List;

@Entity(name = "Flower")
@Table(name = "flower")
public class Flower {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "flower_id")
    private long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "name", nullable = false)
    private FlowerName name;

    @Column(name = "length", nullable = false)
    private short length;

    @Enumerated(EnumType.STRING)
    @Column(name = "color", nullable = false)
    private FlowerColor color;

    @Column(name = "price", nullable = false)
    private double price;

    @Column(name = "quality", nullable = false)
    private int quality;

    @OneToMany(mappedBy = "flower", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<FlowerInBouquet> flowerInBouquets;


    public Flower(FlowerName name, short length, FlowerColor color, double price, int quality) {
        this.name = name;
        this.length = length;
        this.color = color;
        this.price = price;
        this.quality = quality;
    }

    public Flower() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public FlowerName getName() {
        return name;
    }

    public void setName(FlowerName name) {
        this.name = name;
    }

    public short getLength() {
        return length;
    }

    public void setLength(short length) {
        this.length = length;
    }

    public FlowerColor getColor() {
        return color;
    }

    public void setColor(FlowerColor color) {
        this.color = color;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuality() {
        return quality;
    }

    public void setQuality(int quality) {
        this.quality = quality;
    }

    public List<FlowerInBouquet> getFlowerInBouquets() {
        return flowerInBouquets;
    }

    public void setFlowerInBouquets(List<FlowerInBouquet> flowerInBouquets) {
        this.flowerInBouquets = flowerInBouquets;
    }

    @Override
    public String toString() {
        return "Flower{" +
                "id=" + id +
                ", name=" + name +
                ", length=" + length +
                ", color=" + color +
                ", price=" + price +
                ", quality=" + quality +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}